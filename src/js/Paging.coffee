$ = require('jquery')
Events = require('sk-events')

class Paging extends Events
	constructor: (widget, options)->
		super

		@widget = widget
		@options = $.extend(
			'min': 0
			'max': 1
			'step': 1
			'infinite': false
			'getCurrent': ()->
				return 0
		, options)

		@min = @options.min
		@max = @options.max
		@step = @options.step

		@getCurrent = @options.getCurrent
		# console.log(@options.getCurrent)
		@currentIndex = @getCurrent()
		# @callback = callback
		@isInfinite = @options.infinite

	_goTo: (condition, index, action)->
		callback = 'cancel'
		if condition
			callback = 'change'
			index = @getIndex(index)

		@currentIndex = index

		@trigger(callback,
			'action': action
			'index': index
		)

	getIndex: (index, infinite) ->
		infinite ?= @isInfinite

		if infinite
			index = (index + (@max + 1)) % (@max + 1)
		else
			index = if index < @min then @min else if index > @max then @max else index

	prev: ()->
		@currentIndex = @getCurrent()
		condition = @min isnt @currentIndex or @isInfinite
		index = @currentIndex - @step
		@_goTo(condition, index, 'prev')

	next: ()->
		@currentIndex = @getCurrent()
		condition = @max isnt @currentIndex or @isInfinite
		index = @currentIndex + @step
		@_goTo(condition, index, 'next')

	goTo: (index)->
		@currentIndex = @getCurrent()
		condition = @currentIndex isnt index
		@_goTo(condition, index, 'goto')


module.exports = Paging


# sk.utils.PagingMixin =
# 	initPaging: (options)->
# 		@paging = new sk.utils.Paging(@, options)
# 		@

# 	prev: ()->
# 		@paging.prev()
# 		@

# 	next: ()->
# 		@paging.next()
# 		@

# 	goTo: (index)->
# 		@paging.goTo(index)
# 		@
